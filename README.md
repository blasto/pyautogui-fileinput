PyAutoGUI-fileinput
===================

These are additions to PyAutoGUI that make it easier to retype batches of text from files or lists in python. Reduce errors with repetative data entry tasks, especially for programs that don't support copy and paste like ncurses terminal interfaces. The main use is for files that are a list items to enter with one per line. The _alternatelinewith_ optional parameter lets you end each item with something like _return_ or _tab_.

Forked from PyAutoGUI, hosted at: https://github.com/asweigart/pyautogui. See this for more details on basic usage.

